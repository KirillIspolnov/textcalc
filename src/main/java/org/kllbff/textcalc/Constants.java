package org.kllbff.textcalc;

public interface Constants {
    public static final String KEY_FIRST_VALUE = "first-value";
    public static final String KEY_SECOND_VALUE = "second-value";
    public static final String KEY_OPERATION = "operation-enum";
    public static final String KEY_DATA_OBJECT = "data-object";
    public static final String KEY_SIMPLE_MODE = "simple-mode-used";
    public static final String KEY_START_TIME = "start-time";
}
