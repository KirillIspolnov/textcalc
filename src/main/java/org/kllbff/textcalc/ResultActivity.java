package org.kllbff.textcalc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class ResultActivity extends AppCompatActivity implements Constants {
    private TextView resultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        resultView = findViewById(R.id.value_result);

        Intent intent = getIntent();
        if(intent == null) {
            Snackbar.make(resultView, R.string.error_invalid_intent, Snackbar.LENGTH_LONG).show();
        } else {
            long time = intent.getLongExtra(KEY_START_TIME, 0L);
            Snackbar.make(resultView, getString(R.string.message_time, (System.currentTimeMillis() - time) / 1000.0f), Snackbar.LENGTH_LONG).show();

            double first, second;
            Operation operation;

            if(intent.getBooleanExtra(KEY_SIMPLE_MODE, true)) {
                first = intent.getDoubleExtra(KEY_FIRST_VALUE, 0);
                second = intent.getDoubleExtra(KEY_SECOND_VALUE, 0);
                operation = (Operation)intent.getSerializableExtra(KEY_OPERATION);
            } else {
                Data data = (Data)intent.getSerializableExtra(KEY_DATA_OBJECT);
                first = data.getFirstValue();
                second = data.getSecondValue();
                operation = data.getOperation();
            }

            String operationString;
            double result;
            switch(operation) {
                case SUBTRACTION:
                    operationString = " - ";
                    result = first - second;
                break;
                case SUMMATION:
                    operationString = " + ";
                    result = first + second;
                break;
                default:
                    Log.i("ResultActivity", "Hey man, do you want to implement this feature fully?!");
                    finish();
                    return;
            }

            resultView.setText(stringify(first, second, operationString, result));
        }

        if(savedInstanceState != null) {
            resultView.setText(savedInstanceState.getString(KEY_DATA_OBJECT));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);

        bundle.putString(KEY_DATA_OBJECT, resultView.getText().toString());
    }

    private String stringify(double first, double second, String operation, double result) {
        StringBuilder builder = new StringBuilder();
        appendNumber(builder, first);

        builder.append(operation);

        if(second < 0) {
            builder.append("(");
        }
        appendNumber(builder, second);
        if(second < 0) {
            builder.append(")");
        }

        builder.append(" = ");
        appendNumber(builder, result);

        return builder.toString();
    }

    private void appendNumber(StringBuilder builder, double number) {
        if(number % 1.0 == 0.0) {
            builder.append((int)number);
        } else {
            builder.append(number);
        }
    }
}
