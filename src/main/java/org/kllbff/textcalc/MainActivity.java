package org.kllbff.textcalc;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements Constants {
    private TextView firstValueField, secondValueField;
    private Switch useSimpleModeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstValueField = findViewById(R.id.value_first);
        secondValueField = findViewById(R.id.value_second);
        useSimpleModeView = findViewById(R.id.flag_simple_mode);

        if(savedInstanceState != null) {
            firstValueField.setText(savedInstanceState.getString(KEY_FIRST_VALUE));
            secondValueField.setText(savedInstanceState.getString(KEY_SECOND_VALUE));
            useSimpleModeView.setChecked(savedInstanceState.getBoolean(KEY_SIMPLE_MODE));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);

        bundle.putString(KEY_FIRST_VALUE, firstValueField.getText().toString());
        bundle.putString(KEY_SECOND_VALUE, firstValueField.getText().toString());
        bundle.putBoolean(KEY_SIMPLE_MODE, useSimpleModeView.isChecked());
    }

    private void showSnackbar(int resId, Object... values) {
        Snackbar.make(firstValueField, getString(resId, values), Snackbar.LENGTH_LONG).show();
    }

    private boolean checkInputData(TextView view, String name) {
        String text = view.getText().toString();

        if(text.isEmpty()) {
            showSnackbar(R.string.error_empty, name);
            return false;
        }
        if(text.matches("(\\d)|(\\d+(\\.)?\\d+)")) {
            return true;
        }

        showSnackbar(R.string.error_unsupported, name);
        return false;
    }

    public void onSubmitClick(View view) {
        Double first, second;

        if(!checkInputData(firstValueField, getString(R.string.field_first)) ||
           !checkInputData(secondValueField, getString(R.string.field_second))) {
            return; //fail
        }

        first = Double.parseDouble(firstValueField.getText().toString());
        second = Double.parseDouble(secondValueField.getText().toString());

        Operation operation;

        switch(view.getId()) {
            case R.id.action_sum: operation = Operation.SUMMATION; break;
            default:
                Log.i("MainActivity", "Hey man, do you want to implement this feature fully?!");
                return;
        }

        Intent intent = new Intent(this, ResultActivity.class);
        boolean useSimpleMode = useSimpleModeView.isChecked();
        intent.putExtra(KEY_START_TIME, System.currentTimeMillis());
        if(useSimpleMode) {
            intent.putExtra(KEY_FIRST_VALUE, first);
            intent.putExtra(KEY_SECOND_VALUE, second);
            intent.putExtra(KEY_OPERATION, operation);
        } else {
            intent.putExtra(KEY_DATA_OBJECT, new Data(first, second, operation));
        }
        intent.putExtra(KEY_SIMPLE_MODE, useSimpleMode);

        startActivity(intent);
    }
}
