package org.kllbff.textcalc;

import java.io.Serializable;

public class Data implements Serializable {
    private double first;
    private double second;
    private Operation operation;

    public Data(double first, double second, Operation operation) {
        this.first = first;
        this.second = second;
        this.operation = operation;
    }

    public double getFirstValue() {
        return first;
    }

    public double getSecondValue() {
        return second;
    }

    public Operation getOperation() {
        return operation;
    }
}
