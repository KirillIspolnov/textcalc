package org.kllbff.textcalc;

public enum Operation {
    SUMMATION, SUBTRACTION
}
